import { Egreso } from './egreso.model';

export class EgresoServicio{
    egresos:Egreso[]=[
        new Egreso("Mercancia Cosmeticos",7000),
        new Egreso("Saco de harina",2000)
    ];

    eliminar(egreso:Egreso){
        const indice: number = this.egresos.indexOf(egreso);
        this.egresos.splice(indice,1);
      }
}