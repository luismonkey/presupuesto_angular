import { Ingreso } from './ingreso.model';

export class IngresoServicio{
    ingresos: Ingreso[]=[
      new Ingreso("Venta de cosmeticos",4000),
      new Ingreso("Venta de harina",9000)
    ];

    eliminar(ingreso: Ingreso){
      const indice: number = this.ingresos.indexOf(ingreso);
      this.ingresos.splice(indice,1);
    }
}